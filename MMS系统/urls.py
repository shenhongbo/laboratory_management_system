"""MMS系统 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from app01 import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # 实验室显示、添加、删除、修改路由
    url(r'^laboratory/', views.laboratory),
    url(r'^laboratory_add/', views.laboratory_add),
    url(r'^laboratory_del/', views.laboratory_del, name='laboratory_del'),
    url(r'^laboratory_alter/', views.laboratory_alter),

    # 物资显示、添加、删除、修改路由
    url(r'^material/', views.material),
    url(r'^material_del/', views.material_del, name='material_del'),
    url(r'^material_alter/', views.material_alter),
    url(r'^material_add/', views.material_add),

    # 用户显示、添加、删除、修改路由
    url(r'^user/', views.user),
    url(r'^user_del/', views.user_del, name='user_del'),
    url(r'^user_alter/', views.user_alter),
    url(r'^user_add/', views.user_add),

    url(r'^index/', views.index),

]
