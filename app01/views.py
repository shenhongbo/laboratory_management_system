from django.shortcuts import render, redirect, HttpResponse
from app01 import models
from django import forms
from django.http.response import JsonResponse


# Create your views here.


# 展示功能
def laboratory(request):
    laboratory_obj = models.Laboratory.objects.all()
    return render(request, 'laboratory.html', {'laboratory_obj': laboratory_obj})


def material(request):
    material_all = models.Material.objects.all().values('mater_num', 'material_name', 'buy_time', 'l_id__l_name', 'id')
    return render(request, 'material.html', {'material_all': material_all})


def user(request):
    user_all = models.User.objects.all()
    return render(request, 'user.html', {'user_all': user_all})


# 修改功能
def laboratory_alter(request):
    l_id = request.GET.get('id')
    laboratory_obj = models.Laboratory.objects.get(pk=l_id)
    if request.method == 'POST':
        l_name = request.POST.get('name')
        layer_num = request.POST.get('layer')
        room_num = request.POST.get('room')
        lab_manage = request.POST.get('manage')

        laboratory_obj.l_name = l_name
        laboratory_obj.layer_num = layer_num
        laboratory_obj.room_num = room_num
        laboratory_obj.lab_manage = lab_manage
        laboratory_obj.save()
        return redirect('/laboratory/')
    return render(request, 'laboratory_alter.html', {'laboratory_obj': laboratory_obj})


def material_alter(request):
    mater_id = request.GET.get('id')
    # 通过要修改行的id筛选出所对应的对象
    material_obj = models.Material.objects.get(id=mater_id)
    # 获取物资表的对象
    lab_obj = models.Laboratory.objects.all()
    if request.method == 'POST':
        mater_num = request.POST.get('mater_num')
        material_name = request.POST.get('material_name')
        buy_time = request.POST.get('buy_time')
        l_id = request.POST.get('l_id')
        models.Material.objects.create(mater_num=mater_num, material_name=material_name, buy_time=buy_time,
                                       l_id_id=l_id)
        return redirect('/material/')
    return render(request, 'material_alter.html', {'material_obj': material_obj, 'lab_obj': lab_obj})


def user_alter(request):
    user_id = request.GET.get('id')
    user_all = models.User.objects.get(id=user_id)
    if request.method == 'POST':
        nickname = request.POST.get('nickname')
        username = request.POST.get('username')
        phone = request.POST.get('phone')
        models.User.objects.create(nickname=nickname, username=username, phone=phone)
        return redirect('/user/')
    return render(request, 'user_alter.html', {'user_all': user_all})


# 添加功能
class RegForm(forms.Form):
    l_name = forms.CharField(label='实验室', required=True, error_messages={'required': '不能为空'})
    layer_num = forms.CharField(label='楼层')
    room_num = forms.CharField(label='门牌号')
    lab_manage = forms.CharField(label='负责人')


def laboratory_add(request):
    if request.method == 'POST':
        lab_obj = RegForm(request.POST)
        if lab_obj.is_valid():
            l_name = lab_obj.cleaned_data.get('l_name')
            layer_num = lab_obj.cleaned_data.get('layer_num')
            room_num = lab_obj.cleaned_data.get('room_num')
            lab_manage = lab_obj.cleaned_data.get('lab_manage')
            models.Laboratory.objects.create(l_name=l_name, layer_num=layer_num, room_num=room_num,
                                             lab_manage=lab_manage)
            return redirect('/laboratory/')
    form_obj = RegForm()
    return render(request, 'laboratory_add.html', {'form_obj': form_obj})


class MaterForm(forms.Form):
    material_name = forms.CharField(label='物资名称', required=True, error_messages={'required': '不能为空'})
    mater_num = forms.CharField(label='物资编号')
    mater_desc = forms.CharField(label='物资描述', widget=forms.TextInput)
    buy_time = forms.DateTimeField(label='购买时间')
    l_id = forms.ChoiceField(label='所属实验室', choices=models.Laboratory.objects.values_list('id', 'l_name'))


def material_add(request):
    if request.method == 'POST':
        mater_obj = MaterForm(request.POST)
        if mater_obj.is_valid():
            material_name = mater_obj.cleaned_data.get('material_name')
            mater_num = mater_obj.cleaned_data.get('mater_num')
            mater_desc = mater_obj.cleaned_data.get('mater_desc')
            buy_time = mater_obj.cleaned_data.get('buy_time')
            l_id = mater_obj.cleaned_data.get('l_id')
            models.Material.objects.create(material_name=material_name, mater_num=mater_num, mater_desc=mater_desc,
                                           buy_time=buy_time, l_id_id=l_id)
            return redirect('/material/')
    mater_form_obj = MaterForm()
    return render(request, 'material_add.html', {'mater_form_obj': mater_form_obj})


class UserForm(forms.Form):
    nickname = forms.CharField(label='昵称')
    username = forms.CharField(label='用户名')
    password = forms.CharField(label='密码')
    phone = forms.CharField(label='电话号码')


def user_add(request):
    if request.method == 'POST':
        user_obj = UserForm(request.POST)
        if user_obj.is_valid():
            nickname = user_obj.cleaned_data.get('nickname')
            username = user_obj.cleaned_data.get('username')
            password = user_obj.cleaned_data.get('password')
            phone = user_obj.cleaned_data.get('phone')
            models.User.objects.create(nickname=nickname, username=username, password=password, phone=phone)
            return redirect('/user/')
    user_form_obj = UserForm()
    return render(request, 'user_add.html', {'user_form_obj': user_form_obj})


# 删除操作
def laboratory_del(request):
    l_id = request.GET.get('id')
    models.Laboratory.objects.filter(pk=l_id).delete()
    return JsonResponse({'mag': 'ok', 'status': 200})


def material_del(request):
    mater_id = request.GET.get('id')
    models.Material.objects.filter(pk=mater_id).delete()
    return JsonResponse({'mag': 'ok', 'status': 200})


def user_del(request):
    user_id = request.GET.get('id')
    models.User.objects.filter(id=user_id).delete()
    return JsonResponse({'mag': 'ok', 'status': 200})


def index(request):
    return render(request, 'index.html')
