from django.db import models

# Create your models here.

# 实验室表 物资表 用户表  实验室表包含内容：名称、楼层、房间号、负责人
# 资产表包含内容：物资名称、物资描述、物资编号、购买时间、购买人
# 用户表包含内容：用户名、密码、姓名、手机号


class Laboratory(models.Model):     # 实验室表
    l_name = models.CharField(max_length=32, verbose_name='实验室名')
    layer_num = models.CharField(max_length=32, verbose_name='楼层数')
    room_num = models.IntegerField(verbose_name='房间号')
    lab_manage = models.CharField(max_length=32, verbose_name='实验室管理员')


class Material(models.Model):   # 物资表
    material_name = models.CharField(max_length=32, verbose_name='物资名称')
    mater_desc = models.CharField(max_length=255, verbose_name='物资描述')
    mater_num = models.CharField(max_length=32, verbose_name='物资编号')
    buy_time = models.DateTimeField(auto_now_add=True, null=True, verbose_name='购买时间')
    l_id = models.ForeignKey('Laboratory', on_delete=models.CASCADE)
    user_id = models.ManyToManyField('User')


class User(models.Model):   # 用户表
    nickname = models.CharField(max_length=32)
    username = models.CharField(max_length=32)
    password = models.CharField(max_length=32)
    phone = models.CharField(max_length=32)



























